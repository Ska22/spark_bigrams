Задача состоит в извлечении коллокаций. 
Это комбинации слов, которые часто встречаются вместе. Например, «High school» или «Roman Empire». Чтобы найти совпадения, нужно использовать метрику NPMI (нормализованная точечная взаимная информация).
PMI двух слов a и b определяется как:

PMI(a,b)=ln(P(a,b)P(a)∗P(b))\textcolor{blue}{PMI \lparen a,b \rparen = ln \bigg( {\frac {P(a,b)}{P(a)*P(b)}} \bigg)}PMI(a,b)=ln(P(a)∗P(b)P(a,b)​)


, где P(ab) - вероятность двух слов, идущих подряд, а P(a) и P (b) - вероятности слов a и b соответственно.
Вам нужно будет оценить вероятности встречаемости слов, то есть:

P(a)=num_of_occurrences_of_word_"a"num_of_occurrences_of_all_wordsP(a)=\frac{num\_of\_occurrences\_of\_word\_"a"}{num\_of\_occurrences\_of\_all\_words}P(a)=num_of_occurrences_of_all_wordsnum_of_occurrences_of_word_"a"​



P(ab)=num_of_occurrences_of_pair_"ab"num_of_occurrences_of_all_pairsP(ab)=\frac{num\_of\_occurrences\_of\_pair\_"ab"}{num\_of\_occurrences\_of\_all\_pairs}P(ab)=num_of_occurrences_of_all_pairsnum_of_occurrences_of_pair_"ab"​




total_number_of_words - общее кол-во слов в тексте

total_number_of_word_pairs - общее кол-во пар
"Roman Empire"; предположим, что это уникальная комбинация, и за каждым появлением «Roman» следует «Empire», и, наоборот, каждому появлению «Empire» предшествует «Roman». В этом случае «P (ab) = P (a) = P(b)», поэтому «PMI (a, b) = -lnP(a) = -lnP(b)». Чем реже встречается эта коллокация, тем больше значение PMI.
"the doors"; предположим, что «the» может встретится рядом с любым словом. Таким образом, «P (ab) = P (a) * P (b)» и «PMI (a, b) = ln 1 = 0".
"green idea / sleeps furiously"; когда два слова никогда не встречаются вместе, «P (ab) = 0» и «PMI (a, b) = -inf».

NPMI вычисляется как NPMI(a,b)=−PMI(a,b)lnP(a,b)NPMI(a,b)=-\frac{PMI(a,b)}{lnP(a,b)}NPMI(a,b)=−lnP(a,b)PMI(a,b)​ . Это нормализует величину в диапазон [-1; 1].
Найти самые популярные коллокации в Википедии. Обработка данных:

При парсинге отбрасывайте все символы, которые не являются латинскими буквами: text = re.sub("^\W+|\W+$", "", text)

приводим все слова к нижнему регистру;
удаляем все стоп-слова (даже внутри биграммы т.к. “at evening” имеет ту же семантику что и “at the evening”);
биграммы объединить символом нижнего подчеркивания "_";
работаем только с теми биграммами, которые встретились не реже 500 раз (т.е. проводим все необходимые join'ы и считаем NPMI только для них).
общее число слов и биграмм считать до фильтрации.

Для каждой биграммы посчитать NPMI и вывести на экран (в STDOUT) TOP-39 самых популярных коллокаций, отсортированных по убыванию значения NPMI. Само значение NPMI выводить не нужно.
Пример вывода

roman_empire
south_africa


Пример вывода на sample-датасете (со значениями NPMI):

19th_century	0.757464166177
20th_century	0.751460453349
references_external	0.731826941011
soviet_union	0.727806412183
air_force	0.705773204264
baseball_player	0.691711138551
university_press	0.687424532005
roman_catholic	0.683677693663
united_kingdom	0.68336461567
